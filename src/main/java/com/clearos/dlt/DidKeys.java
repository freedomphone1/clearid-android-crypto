package com.clearos.dlt;

import com.goterl.lazycode.lazysodium.utils.KeyPair;

public class DidKeys {
    public final String did;
    public final String verKey;
    public final KeyPair keys;

    DidKeys(String DID, String VK, KeyPair keyPair) {
        did = DID;
        verKey = VK;
        keys = keyPair;
    }
}
