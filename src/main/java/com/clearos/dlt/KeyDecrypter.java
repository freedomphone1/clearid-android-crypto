package com.clearos.dlt;

import android.content.Context;

import com.goterl.lazycode.lazysodium.exceptions.SodiumException;

import io.github.novacrypto.SecureCharBuffer;

public class KeyDecrypter {
    private Context appContext;
    private KeyRestorer restorer;

    public KeyDecrypter(Context context, KeyRestorer keyRestorer) {
        appContext = context;
        restorer = keyRestorer;
    }

    public void onKeyDecryptSuccess(SecureCharBuffer seedWord) {
        try {
            restorer.onRestoreSuccess(new CryptoKey(appContext, seedWord, false));
        } catch (SodiumException e) {
            restorer.onRestoreError(e);
        }
    }

    public void onKeyDecryptFailure(Exception e) {
        restorer.onRestoreError(e);
    }
}
