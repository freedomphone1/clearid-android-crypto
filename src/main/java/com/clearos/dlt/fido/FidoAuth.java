package com.clearos.dlt.fido;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;

import com.clearos.dlt.CryptoKey;
import com.clearos.dlt.DidKeys;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import okhttp3.Call;

import com.google.android.gms.fido.Fido;
import com.google.android.gms.fido.fido2.api.common.*;
import com.google.android.gms.fido.fido2.Fido2ApiClient;
import com.google.android.gms.fido.fido2.Fido2PendingIntent;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.gson.JsonObject;

import org.json.JSONObject;

public class FidoAuth implements AuthenticateBeginCallback, RegisterBeginCallback {

    private Fido2ApiClient fidoClient;
    private String lastKnownChallenge;
    private Context appContext;
    private Executor executor;
    private AuthApi api;
    private MutableLiveData<Boolean> processing = new MutableLiveData<>();
    private DidKeys lastKnownSigningKeys;
    private RegistrationHandler registration;
    private AuthenticationHandler authentication;
    private ResponseCallback responseCallback;

    public FidoAuth(Context context, CryptoKey keys) {
        appContext = context;
        executor = Executors.newFixedThreadPool(64);
        api = new AuthApi(context, keys);
        fidoClient = Fido.getFido2ApiClient(context);
    }

    @Override
    public void onSuccess(PublicKeyCredentialCreationOptions options, String challenge) {
        lastKnownChallenge = challenge;
        MutableLiveData<Fido2PendingIntent> result = new MutableLiveData<>();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Task<Fido2PendingIntent> task = fidoClient.getRegisterIntent(options);
                    result.postValue(Tasks.await(task));
                } catch (Exception e) {
                    Log.e("FIDO-R", "During registration complete", e);
                } finally {
                    processing.postValue(false);
                }
                registration.onBeginSuccess(result);
            }
        });

    }



    @Override
    public void onSuccess(PublicKeyCredentialRequestOptions options, String challenge) {
        lastKnownChallenge = challenge;
        MutableLiveData<Fido2PendingIntent> result = new MutableLiveData<>();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Task<Fido2PendingIntent> task = fidoClient.getSignIntent(options);
                    result.postValue(Tasks.await(task));
                } catch (Exception e) {
                    Log.e("FIDO-A", "During authentication complete", e);
                } finally {
                    processing.postValue(false);
                }
                authentication.onAuthBeginSuccess(result);
            }
        });
    }

    @Override
    public void onFailure(Call call, Exception e) {
        Log.e("FIDO-RA", "Failure during API call.", e);
        processing.postValue(false);
    }

    public Call registerBegin(DidKeys signingKeys, RegistrationHandler registrationHandler) throws Exception {
        lastKnownSigningKeys = signingKeys;
        registration = registrationHandler;
        return api.apiRegisterBegin(signingKeys, this);
    }

    public Call authenticateBegin(DidKeys signingKeys, AuthenticationHandler authenticationHandler,
                                  AuthExtras extras) throws Exception {
        lastKnownSigningKeys = signingKeys;
        authentication = authenticationHandler;
        return api.apiAuthenticateBegin(signingKeys, extras,this);
    }


    public Call vaultKeysBegin(DidKeys signingKeys, ResponseCallback responseCallback) throws Exception {
        lastKnownSigningKeys = signingKeys;
        return api.apiGetStoredPassword(signingKeys, responseCallback);

    }

    public Call passwordBegin(DidKeys signingKeys,String url, String key, ResponseCallback responseCallback) throws Exception {
        lastKnownSigningKeys = signingKeys;
        return api.apiGetPasswordDetails(signingKeys, url, key, responseCallback);

    }


    public Call mobileFormBegin(DidKeys signingKeys, JsonObject jsonObject, ResponseCallback responseCallback, int code) throws Exception {
        lastKnownSigningKeys = signingKeys;
        return api.apiGetDetails(signingKeys, jsonObject, responseCallback, code);

    }


    public Call mobileStoreBegin(DidKeys signingKeys, JsonObject jsonObject, ResponseCallback responseCallback, int code) throws Exception {
        lastKnownSigningKeys = signingKeys;
        return api.apiAddStore(signingKeys, jsonObject, responseCallback, code);

    }

    public Call mobileRemove(DidKeys signingKeys, String key, String category, String provider, ResponseCallback responseCallback, int code) throws Exception {
        lastKnownSigningKeys = signingKeys;
        return api.apiRemoveStore(signingKeys, key, category, provider, responseCallback, code);

    }

    public void registerComplete(Intent data) {

        processing.postValue(true);
        byte[] errorExtra = data.getByteArrayExtra(Fido.FIDO2_KEY_ERROR_EXTRA);
        if (errorExtra != null) {
            AuthenticatorErrorResponse error = AuthenticatorErrorResponse.deserializeFromBytes(errorExtra);
            Toast.makeText(appContext, error.getErrorMessage(), Toast.LENGTH_LONG).show();
            Log.e("FIDO-RC", error.getErrorMessage());
            processing.postValue(false);
        } else {
            try {
                String challenge = lastKnownChallenge;
                AuthenticatorAttestationResponse response = AuthenticatorAttestationResponse.deserializeFromBytes(
                    data.getByteArrayExtra(Fido.FIDO2_KEY_RESPONSE_EXTRA));
                Call complete = api.apiRegisterComplete(lastKnownSigningKeys, challenge, response, registration);
                registration.onCompleteStarted(complete);
            } catch (Exception e) {
                Log.e("FIDO-R", "Cannot call registerResponse", e);
            } finally {
                processing.postValue(false);
            }
        }
    }

    public void authenticateComplete(Intent data) {
        processing.postValue(true);
        byte[] errorExtra = data.getByteArrayExtra(Fido.FIDO2_KEY_ERROR_EXTRA);
        if (errorExtra != null) {
            AuthenticatorErrorResponse error = AuthenticatorErrorResponse.deserializeFromBytes(errorExtra);
            Toast.makeText(appContext, error.getErrorMessage(), Toast.LENGTH_LONG).show();
            Log.e("FIDO-AC", error.getErrorMessage());
            processing.postValue(false);
        } else {
            try {
                String challenge = lastKnownChallenge;
                AuthenticatorAssertionResponse response = AuthenticatorAssertionResponse.deserializeFromBytes(
                        data.getByteArrayExtra(Fido.FIDO2_KEY_RESPONSE_EXTRA)
                );
                Call complete = api.apiAuthenticateComplete(lastKnownSigningKeys, challenge, response, authentication);

                Log.d("TAG", "authenticateComplete: "+complete);

                authentication.onAuthCompleteStarted(complete);
            } catch (Exception e) {
                Log.e("FIDO-A", "Cannot call authenticationResponse", e);
            } finally {
                processing.postValue(false);
            }
        }
    }


}
