package com.clearos.dlt.fido;

import com.google.android.gms.fido.fido2.api.common.PublicKeyCredentialCreationOptions;

import okhttp3.Call;
import okhttp3.Response;

public interface ResponseCallback {

    void onResponseSuccess(Call call, Response response, int code);
    void onResponseFailure(Call call, Exception e);

}
