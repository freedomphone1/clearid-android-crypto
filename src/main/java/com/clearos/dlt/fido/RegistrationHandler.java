package com.clearos.dlt.fido;

import androidx.lifecycle.LiveData;

import com.google.android.gms.fido.fido2.Fido2PendingIntent;

import okhttp3.Call;

public interface RegistrationHandler extends ListKeysCallback {
    void onBeginSuccess(LiveData<Fido2PendingIntent> intent);
    void onCompleteStarted(Call apiCall);
}
