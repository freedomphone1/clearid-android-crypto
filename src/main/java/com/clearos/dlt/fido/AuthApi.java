package com.clearos.dlt.fido;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.Pair;
import android.widget.Toast;
import com.clearos.dlt.CryptoKey;
import com.clearos.dlt.DataCustodian;
import com.clearos.dlt.DidKeys;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.databind.CBORMapper;
import com.google.android.gms.fido.fido2.api.common.Attachment;
import com.google.android.gms.fido.fido2.api.common.AuthenticatorAssertionResponse;
import com.google.android.gms.fido.fido2.api.common.AuthenticatorAttestationResponse;
import com.google.android.gms.fido.fido2.api.common.AuthenticatorSelectionCriteria;
import com.google.android.gms.fido.fido2.api.common.COSEAlgorithmIdentifier;
import com.google.android.gms.fido.fido2.api.common.PublicKeyCredentialCreationOptions;
import com.google.android.gms.fido.fido2.api.common.PublicKeyCredentialDescriptor;
import com.google.android.gms.fido.fido2.api.common.PublicKeyCredentialParameters;
import com.google.android.gms.fido.fido2.api.common.PublicKeyCredentialRequestOptions;
import com.google.android.gms.fido.fido2.api.common.PublicKeyCredentialRpEntity;
import com.google.android.gms.fido.fido2.api.common.PublicKeyCredentialType;
import com.google.android.gms.fido.fido2.api.common.PublicKeyCredentialUserEntity;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import static com.clearos.dlt.DataCustodian.JSON;

/**
 * Interacts with the server API.
 */
class AuthApi {
    private Context appContext;
    private DataCustodian homeServer;
    private CryptoKey keys;
    private final ObjectMapper MAPPER =  CBORMapper.builder().build();

    private OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(new AddHeaderInterceptor())
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(40, TimeUnit.SECONDS)
            .connectTimeout(40, TimeUnit.SECONDS)
            .build();

    public AuthApi(Context context, CryptoKey cryptoKeys) {
        appContext = context;
        homeServer = cryptoKeys.homeServer;
        keys = cryptoKeys;
    }

    private Call doRequest(Request request, Callback callback) {
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }

    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     */
    public Call getKeys(DidKeys signingKeys, ListKeysCallback callback) throws Exception {
        Map<String, Object> body = new HashMap<>();
        Gson gson = new Gson();
        String json = gson.toJson(body);

        Map<String, String> params = new HashMap<>();
        URI target = homeServer.getURI("/fido/register/list-credentials", params);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);
        RequestBody jBody = RequestBody.create(json, JSON);
        Request request = new Request.Builder().url(target.toURL()).headers(headerbuild).
                post(jBody).build();

        request = keys.signRequest(request, signingKeys);

        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onFailure(call, e);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.code() >= 200 && response.code() < 300) {
                    ResponseBody body = response.body();
                    callback.onListCredentials("getKeys", parseUserCredentials(body));
                }
                else {
                    callback.onFailure(call, new IOException(response.message()));
                }
            }
        });
    }

    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     */
    public Call apiRegisterBegin(DidKeys signingKeys, RegisterBeginCallback callback) throws Exception {

        Map<String, Object> body = new HashMap<>();
        body.put("did", keys.getDid());
        body.put("attestation", "none");

        Map<String, String> selection = new HashMap<>();
        selection.put("authenticatorAttachment", "platform");
        selection.put("userVerification", "required");
        body.put("authenticatorSelection", selection);

        Gson gson = new Gson();
        String json = gson.toJson(body);

        Map<String, String> params = new HashMap<>();
        URI target = homeServer.getURI("/fido/register/begin", params);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);
        RequestBody jBody = RequestBody.create(json, JSON);
        Request request = new Request.Builder().url(target.toURL()).headers(headerbuild).
                post(jBody).build();

        Log.d("request", "apiRegisterBegin: "+request.toString());
        request = keys.signRequest(request, signingKeys);



        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onFailure(call, e);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.code() >= 200 && response.code() < 300) {
                    try {

                        Log.d("SUCCESS: AuthApi", "register begin response: "+response.body());

                        if (response== null){
                            Toast.makeText(appContext, "Server error", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Pair<PublicKeyCredentialCreationOptions, String> result = parsePublicKeyCredentialCreationOptions(response.body());
                        callback.onSuccess(result.first, result.second);
                    } catch (Exception e) {
                        callback.onFailure(call, e);
                    }
                }
                else {
                    Log.d("ERROR: AuthApi", "register begin response: "+response.message());
                    threadSafeToast(appContext, "Server error \n"+"ERROR: AuthApi register begin response: "+response.message());
                    callback.onFailure(call, new IOException(response.message()));
                }
            }
        });
    }


    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param challenge The challenge string returned by [registerRequest].
     * @param response The FIDO2 response object.
     */
    public Call apiRegisterComplete(DidKeys signingKeys, String challenge,
                                 AuthenticatorAttestationResponse response, ListKeysCallback callback) throws Exception {

        String rawId = Base64.getEncoder().encodeToString(response.getKeyHandle());
        Map<String, Object> body = new HashMap<>();
        body.put("attestationObject", response.getAttestationObject());
        body.put("clientDataJSON", response.getClientDataJSON());

        Map<String, String> params = new HashMap<>();
        URI target = homeServer.getURI("/fido/register/complete", params);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();
        headers.put("challenge", challenge);

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);
        ObjectMapper objectMapper = new ObjectMapper(new CBORFactory());
        byte[] cborBytes = null;
        try {
            cborBytes = objectMapper.writeValueAsBytes(body);
        } catch (JsonProcessingException e) {
            Log.e("CBOR", "Error encoding body as CBOR", e);
        }


        Log.d("body", "body: "+body);

        RequestBody jBody = RequestBody.create(cborBytes);

        Log.d("jBody", "jBody: "+jBody.toString());

        Request request = new Request.Builder().url(target.toURL()).headers(headerbuild).
                post(jBody).build();


        Log.d("request: ", "apiRegisterComplete: "+request.toString());

        request = keys.signRequest(request, signingKeys);

        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onFailure(call, e);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.code() >= 200 && response.code() < 300) {
                    ResponseBody body = response.body();
                    Log.d("SUCCESS: AuthAPi", "register complete response: "+body);
                    callback.onListCredentials("registerComplete", parseUserCredentials(body));
                }
                else {
                    Log.d("ERROR: AuthAPi", "register complete response: "+body);
                    callback.onFailure(call, new IOException(response.message()));
                }
            }
        });
    }

    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param credentialId The credential ID to be removed.
     */
    public void removeKey(DidKeys signingKeys, String credentialId, Callback callback) throws Exception {
        Map<String, String> params = new HashMap<>();
        params.put("credentialId", credentialId);
        URI target = homeServer.getURI("/fido/register/delete", params);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);
        RequestBody jBody = RequestBody.create("{}", JSON);
        Request request = new Request.Builder().url(target.toURL()).headers(headerbuild).
                delete(jBody).build();

        request = keys.signRequest(request, signingKeys);

        // Finally make the API endpoint call.
        doRequest(request, callback);
    }

    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     */
    public Call apiAuthenticateBegin(DidKeys signingKeys, AuthExtras extras,
                                  AuthenticateBeginCallback callback) throws Exception {
        Map<String, Object> body = new HashMap<>();
        body.put("did", keys.getDid());

        if (extras != null) {
            body.put("method", extras.method);
            body.put("message_id", extras.messageId);
            body.put("reference", extras.reference);
        }

        Gson gson = new Gson();
        String json = gson.toJson(body);

        Map<String, String> params = new HashMap<>();
        URI target = homeServer.getURI("/fido/authenticate/begin", params);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);
        RequestBody jBody = RequestBody.create(json, JSON);
        Request request = new Request.Builder().url(target.toURL()).headers(headerbuild).
                post(jBody).build();

        request = keys.signRequest(request, signingKeys);

        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onFailure(call, e);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.code() >= 200 && response.code() < 300) {
                    ResponseBody body = response.body();
                    try {
                        Pair<PublicKeyCredentialRequestOptions, String> result = parsePublicKeyCredentialRequestOptions(body);
                        callback.onSuccess(result.first, result.second);
                    } catch (Exception e) {
                        callback.onFailure(call, e);
                    }
                } else {
                    callback.onFailure(call, new IOException(response.message()));
                }
            }
        });
    }

    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param challenge The challenge string returned by [authenticateBegin].
     * @param response The assertion response from FIDO2 API.
     */
    public Call apiAuthenticateComplete(DidKeys signingKeys, String challenge,
                                        AuthenticatorAssertionResponse response,
                                        ListKeysCallback callback) throws Exception {
        Map<String, Object> body = new HashMap<>();
        body.put("clientDataJSON", response.getClientDataJSON());
        body.put("authenticatorData", response.getAuthenticatorData());
        body.put("signature", response.getSignature());
        body.put("userHandle", response.getUserHandle());
        body.put("credentialId", response.getKeyHandle());

        Map<String, String> params = new HashMap<>();
        URI target = homeServer.getURI("/fido/authenticate/complete", params);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();
        headers.put("challenge", challenge);

        // Sign with the specified keys for the DID.
        Headers headerbuild = Headers.of(headers);
        ObjectMapper objectMapper = new ObjectMapper(new CBORFactory());
        byte[] cborBytes = null;
        try {
            cborBytes = objectMapper.writeValueAsBytes(body);
        } catch (JsonProcessingException e) {
            Log.e("CBOR", "Error encoding body as CBOR", e);
        }
        RequestBody jBody = RequestBody.create(cborBytes);
        Request request = new Request.Builder().url(target.toURL()).headers(headerbuild).
                post(jBody).build();

        request = keys.signRequest(request, signingKeys);

        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onFailure(call, e);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.code() >= 200 && response.code() < 300) {
                    ResponseBody body = response.body();
                    callback.onListCredentials("authenticateComplete", parseUserCredentials(body));
                }
                else {
                    callback.onFailure(call, new IOException(response.message()));
                }
            }
        });
    }


    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param callback ResponseHandler.
     */
    public Call apiGetStoredPassword(DidKeys signingKeys, ResponseCallback callback) throws Exception {

        Map<String, String> params = new HashMap<>();
        URI target = appendUri(homeServer.getURI("/fido/mobile/vault-keys", params).toString(), "did="+keys.getDid());

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);

        Request request = new Request.Builder()
                .url(target.toURL())
                .headers(headerbuild)
                .get().build();
        Log.d("TAG", "apiGetStoredPassword: "+request);

        request = keys.signRequest(request, signingKeys);

        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onResponseFailure(call, e);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.code() >= 200 && response.code() < 300) {
                    try {

                        Log.d("SUCCESS: AuthApi ", "Get stored password: "+response.body());
                        callback.onResponseSuccess(call, response,0);
                    } catch (Exception e) {
                        callback.onResponseFailure(call, e);
                    }
                }
                else {
                    Log.d("ERROR: AuthApi ", response.message());
                    callback.onResponseFailure(call, new IOException(response.message()));
                }
            }
        });
    }


    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param callback ResponseHandler.
     */
    public Call apiGetPasswordDetails(DidKeys signingKeys, String url, String key, ResponseCallback callback) throws Exception {

        Map<String, String> params = new HashMap<>();
        URI target = appendUri(homeServer.getURI("/fido/mobile/password", params).toString(), "did="+keys.getDid());
        target = appendUri(target.toString(), "url="+url);
        target = appendUri(target.toString(), "key="+key);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);

        Request request = new Request.Builder()
                .url(target.toURL())
                .headers(headerbuild)
                .get().build();
        Log.d("TAG", "apiPasswordDetail request: "+request);

        request = keys.signRequest(request, signingKeys);

        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onResponseFailure(call, e);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.code() >= 200 && response.code() < 300) {
                    try {

                        Log.d("SUCCESS: AuthApi ", "password deatil: "+response.body());
                        callback.onResponseSuccess(call, response, 0);
                    } catch (Exception e) {
                        callback.onResponseFailure(call, e);
                    }
                }
                else {
                    Log.d("ERROR: AuthApi ", response.message());
                    callback.onResponseFailure(call, new IOException(response.message()));
                }
            }
        });
    }


    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param callback ResponseHandler.
     */
    public Call apiGetDetails(DidKeys signingKeys, JsonObject jsonObject, ResponseCallback callback, int code) throws Exception {

        /*Map<String, Object> body = new HashMap<>();
        body.put("did", keys.getDid());
        body.put("attestation", "none");

        Map<String, String> selection = new HashMap<>();
        selection.put("authenticatorAttachment", "platform");
        selection.put("userVerification", "required");
        body.put("authenticatorSelection", selection);*/

        Gson gson = new Gson();
        String json = gson.toJson(jsonObject);

        Map<String, String> params = new HashMap<>();
        URI target = homeServer.getURI("/fido/mobile/form", params);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);
        RequestBody jBody = RequestBody.create(json, JSON);


        Request request = new Request.Builder().url(target.toURL()).headers(headerbuild).
                post(jBody).build();

        Log.d("request", "apimobileFormBegin request: "+request.toString());
        // request = keys.signRequest(request, signingKeys);



        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onResponseFailure(call, e);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.code() >= 200 && response.code() < 300) {
                    try {

                        Log.d("SUCCESS: AuthApi ", "deatil: "+response.body());
                        callback.onResponseSuccess(call, response, code);
                    } catch (Exception e) {
                        callback.onResponseFailure(call, e);
                    }
                }
                else {
                    Log.d("ERROR: AuthApi ", response.message());
                    callback.onResponseFailure(call, new IOException(response.message()));
                }
            }
        });
    }


    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param callback ResponseHandler.
     */
    public Call apiAddStore(DidKeys signingKeys, JsonObject jsonObject, ResponseCallback callback, int code) throws Exception {

        /*Map<String, Object> body = new HashMap<>();
        body.put("did", keys.getDid());
        body.put("attestation", "none");

        Map<String, String> selection = new HashMap<>();
        selection.put("authenticatorAttachment", "platform");
        selection.put("userVerification", "required");
        body.put("authenticatorSelection", selection);*/

        Gson gson = new Gson();
        String json = gson.toJson(jsonObject);

        Map<String, String> params = new HashMap<>();
        URI target = homeServer.getURI("/fido/mobile/store", params);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);
        RequestBody jBody = RequestBody.create(json, JSON);

        Request request = new Request.Builder().url(target.toURL()).headers(headerbuild).
                post(jBody).build();

        Log.d("request", "apimobileFormBegin request: "+request.toString());
        // request = keys.signRequest(request, signingKeys);

        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onResponseFailure(call, e);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.code() >= 200 && response.code() < 300) {
                    try {

                        Log.d("SUCCESS: AuthApi ", "deatil: "+response.body());
                        callback.onResponseSuccess(call, response, code);
                    } catch (Exception e) {
                        callback.onResponseFailure(call, e);
                    }
                }
                else {
                    Log.d("ERROR: AuthApi ", response.message());
                    callback.onResponseFailure(call, new IOException(response.message()));
                }
            }
        });
    }


    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param callback ResponseHandler.
     */
    public Call apiRemoveStore(DidKeys signingKeys, String key, String category, String provider, ResponseCallback callback, int code) throws Exception {

        /*Map<String, Object> body = new HashMap<>();
        body.put("id", name);
        body.put("category", category);
        body.put("did", signingKeys.did);
        body.put("url", provider);*/

        Map<String, String> params = new HashMap<>();
        URI target = appendUri(homeServer.getURI("/fido/mobile/remove", params).toString(), "key="+key);
        target = appendUri(target.toString(), "category="+category);
        target = appendUri(target.toString(), "did="+keys.getDid());
        target = appendUri(target.toString(), "url="+provider);


        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);


        Request request = new Request.Builder()
                .url(target.toURL())
                .headers(headerbuild)
                .delete().build();

        Log.d("request", "apimobileFormBegin request: "+request.toString());
        // request = keys.signRequest(request, signingKeys);



        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                callback.onResponseFailure(call, e);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.code() >= 200 && response.code() < 300) {
                    try {

                        Log.d("SUCCESS: AuthApi ", "deatil: "+response.body());
                        callback.onResponseSuccess(call, response, code);
                    } catch (Exception e) {
                        callback.onResponseFailure(call, e);
                    }
                }
                else {
                    Log.d("ERROR: AuthApi ", response.message());
                    callback.onResponseFailure(call, new IOException(response.message()));
                }
            }
        });
    }





    /** To add queryParameter in request URL
    * @param uri request uri
    * @param appendQuery Query parameter with value
    */
    public static URI appendUri(String uri, String appendQuery) throws URISyntaxException {
        URI oldUri = new URI(uri);

        String newQuery = oldUri.getQuery();
        if (newQuery == null) {
            newQuery = appendQuery;
        } else {
            newQuery += "&" + appendQuery;
        }

        URI newUri = new URI(oldUri.getScheme(), oldUri.getAuthority(),
                oldUri.getPath(), newQuery, oldUri.getFragment());

        return newUri;
    }

    private Pair<PublicKeyCredentialRequestOptions, String> parsePublicKeyCredentialRequestOptions(ResponseBody body) throws Exception {
        PublicKeyCredentialRequestOptions.Builder builder = new PublicKeyCredentialRequestOptions.Builder();
        Map<String,Object> responseMap = (Map<String,Object>) MAPPER.readValue(body.bytes(), Map.class);
        LinkedHashMap<String, Object> dict = (LinkedHashMap<String, Object>)responseMap.get("publicKey");

        String challenge = Base64.getEncoder().encodeToString((byte[])(dict.get("challenge")));
        builder.setChallenge((byte[])(dict.get("challenge")));
        builder.setAllowList(parseCredentialDescriptors(dict.get("allowCredentials")));
        builder.setRpId(dict.get("rpId").toString());
        builder.setTimeoutSeconds((Integer)dict.get("timeout")/1000.);

        return new Pair<>(builder.build(), challenge);
    }

    private Pair<PublicKeyCredentialCreationOptions, String> parsePublicKeyCredentialCreationOptions(
            ResponseBody body) throws Exception {

        PublicKeyCredentialCreationOptions.Builder builder = new PublicKeyCredentialCreationOptions.Builder();
        byte[] cborBytes = body.bytes();
        Map<String,Object> responseMap = (Map<String,Object>) MAPPER.readValue(cborBytes, Map.class);

        LinkedHashMap<String, Object> dict = (LinkedHashMap<String, Object>)responseMap.get("publicKey");
        String challenge = Base64.getEncoder().encodeToString((byte[])dict.get("challenge"));
        builder.setChallenge((byte[])dict.get("challenge"));
        builder.setUser(parseUser(dict.getOrDefault("user", null)));
        builder.setParameters(parseParameters(dict.getOrDefault("pubKeyCredParams", null)));
        // builder.setAllowList(parseCredentialDescriptors(dict.getOrDefault("allowCredentials", null)));
        builder.setExcludeList(parseCredentialDescriptors(dict.getOrDefault("excludeCredentials", null)));
        builder.setAuthenticatorSelection(parseSelection(dict.getOrDefault("authenticatorSelection", null)));
        builder.setRp(parseRp(dict.getOrDefault("rp", null)));
        builder.setTimeoutSeconds((Integer)dict.get("timeout")/1000.);

        return new Pair<>(builder.build(), challenge);
    }

    private PublicKeyCredentialRpEntity parseRp(Object entry) {
        if (entry != null) {
            Map<String, Object> dict = (Map<String, Object>) entry;
            return new PublicKeyCredentialRpEntity(
                    dict.get("id").toString(),
                    dict.get("name").toString(),
                    null // icon: dict.get("icon").toString()
            );
        } else {
            return null;
        }
    }

    private AuthenticatorSelectionCriteria parseSelection(Object entry) throws Exception {
        if (entry != null) {
            Map<String, Object> dict = (Map<String, Object>) entry;
            if (dict.containsKey("authenticatorAttachment")) {
                AuthenticatorSelectionCriteria.Builder builder = new AuthenticatorSelectionCriteria.Builder();
                builder.setAttachment(Attachment.fromString(dict.get("authenticatorAttachment").toString()));
                return builder.build();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private List<PublicKeyCredentialDescriptor> parseCredentialDescriptors(Object descriptors) throws Exception {
        if (descriptors!= null) {
            List<Object> descList = (List<Object>) descriptors;
            List<PublicKeyCredentialDescriptor> result = new ArrayList<>();
            Map<String, Object> entry;
            for (int i = 0; i < descList.size(); i++) {
                entry = (Map<String, Object>)descList.get(i);
                byte[] id = (byte[])entry.get("id");
                result.add(new PublicKeyCredentialDescriptor(
                        PublicKeyCredentialType.PUBLIC_KEY.toString(),
                        id,
                        /* transports */ null
                ));
            }
            return result;
        } else {
            return null;
        }
    }

    private PublicKeyCredentialUserEntity parseUser(Object entry) {
        if (entry != null) {
            Map<String, Object> dict = (Map<String, Object>)entry;
            return new PublicKeyCredentialUserEntity(
                    (byte[]) dict.get("id"),
                    dict.get("name").toString(),
                    dict.getOrDefault("icon","").toString(),
                    dict.get("displayName").toString()
            );
        } else {
            return null;
        }
    }

    private List<PublicKeyCredentialParameters> parseParameters(Object parameters) throws Exception {
        if (parameters!= null) {
            List<Object> paramList = (List<Object>) parameters;
            List<PublicKeyCredentialParameters> result = new ArrayList<>();
            Map<String, Object> entry;
            for (int i = 0; i < paramList.size(); i++) {
                entry = (Map<String, Object>)paramList.get(i);
                int alg = (int)entry.get("alg");
                String type = entry.get("type").toString();
                try {
                    result.add(new PublicKeyCredentialParameters(type, alg));
                } catch (Exception e) {
                    if (e.getMessage().contains("COSEAlgorithmIdentifier.UnsupportedAlgorithmIdentifierException")) {
                        Log.w("ALG", e.getMessage(), e);
                    }
                }
            }
            return result;
        } else {
            return null;
        }
    }

    private Map<String, String> getPublicKey(Object entry) {
        LinkedHashMap<String, Object> pKey = (LinkedHashMap<String, Object>) entry;
        Map<String, String> result = new HashMap<>();
        Base64.Encoder encoder = Base64.getEncoder();
        for (String key: pKey.keySet()) {
            Object value = pKey.get(key);
            try {
                Integer iValue = (Integer)value;
                result.put(key, iValue.toString());
            } catch (Exception e) {
                byte[] bValue = (byte[])value;
                result.put(key, encoder.encodeToString(bValue));
            }
        }
        return result;
    }

    private List<Credential> parseUserCredentials(ResponseBody body) throws IOException {
        PublicKeyCredentialCreationOptions.Builder builder = new PublicKeyCredentialCreationOptions.Builder();
        byte[] cborBytes = body.bytes();
        Map<String,Object> dict = (Map<String,Object>) MAPPER.readValue(cborBytes, Map.class);

        List<Credential> result = new ArrayList<>();
        if (dict.containsKey("credentials")) {
            List<Object> entries = (List<Object>) dict.get("credentials");
            for (int i = 0; i < entries.size(); i++) {
                Map<String, Object> entry = (Map<String, Object>) entries.get(i);
                result.add(new Credential(
                        Base64.getEncoder().encodeToString((byte[])(entry.get("credId"))),
                        getPublicKey(entry.get("publicKey")).toString()
                ));
            }
        }
        return result;
    }

    public void threadSafeToast(Context context, String text) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, text, Toast.LENGTH_LONG).show();
            }
        });
    }

}
