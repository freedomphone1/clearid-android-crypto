package com.clearos.dlt.fido;

import com.google.android.gms.fido.fido2.api.common.PublicKeyCredentialCreationOptions;

import okhttp3.Call;

public interface RegisterBeginCallback {
    void onSuccess(PublicKeyCredentialCreationOptions options, String challenge);
    void onFailure(Call call, Exception e);
}
