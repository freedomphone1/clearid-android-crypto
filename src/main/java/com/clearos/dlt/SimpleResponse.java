package com.clearos.dlt;

public class SimpleResponse {
    public final int code;
    public final String message;

    SimpleResponse(int responseCode, String responseMessage) {
        code = responseCode;
        message = responseMessage;
    }
}