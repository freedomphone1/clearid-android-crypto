package com.clearos.dlt;

public interface KeyRestorer {
    void onRestoreSuccess(CryptoKey cryptoKey);
    void onRestoreError(Exception e);
}
