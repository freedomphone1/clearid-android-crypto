package com.clearos.dlt;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Set;

public class SharedPrefs {
    private static SharedPrefs instance = new SharedPrefs();
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    private SharedPrefs() {} //prevent creating multiple instances by making the constructor private

    //The context passed into the getInstance should be application level context.
    public static SharedPrefs getInstance(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
            editor = sharedPreferences.edit();
        }
        return instance;
    }

    /**
     * Saves a key-value pair to the app's private shared preferences.
     */
    public void saveValue(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    /**
     * Returns a value stored using `saveValue`.
     * @param key Key used to store the value.
     */
    public String getValue(String key) {
        return sharedPreferences.getString(key, null);
    }


    /**
     * Saves a set of strings under a single key.
     */
    public void saveSet(String key, Set<String> value) {
        editor.putStringSet(key, value);
        editor.commit();
    }

    /**
     * Returns a set of strings stored using `saveSet`.
     */
    public Set<String> getSet(String key) {
        return sharedPreferences.getStringSet(key, null);
    }

    /**
     * Removes a value from shared preferences for a String or StringSet.
     */
    public void rmValue(String key) {
        editor.remove(key);
        editor.commit();
    }

    /**
     * Clears all shared preferences for the package.
     */
    public void clearAll() {
        editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
        Log.d("TAG", "data from local storage cleared: ");
    }

}
