package com.clearos.dlt;

import android.app.Activity;
import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.util.Log;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.security.keystore.UserNotAuthenticatedException;
import android.widget.Toast;

import com.clearos.dlt.biometric.BiometricCallback;
import com.clearos.dlt.biometric.BiometricManager;
import com.goterl.lazycode.lazysodium.LazySodiumAndroid;
import com.goterl.lazycode.lazysodium.SodiumAndroid;
import com.goterl.lazycode.lazysodium.exceptions.SodiumException;
import com.goterl.lazycode.lazysodium.interfaces.Box;
import com.goterl.lazycode.lazysodium.interfaces.Random;
import com.goterl.lazycode.lazysodium.interfaces.SecretBox;
import com.goterl.lazycode.lazysodium.utils.Key;
import com.goterl.lazycode.lazysodium.utils.KeyPair;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.spec.ECGenParameterSpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Arrays;
import java.util.Map;

import javax.crypto.Cipher;

import io.github.novacrypto.SecureCharBuffer;

public class SecureStore implements BiometricCallback {
    private KeyStore ks;
    public String storagePath;
    public List<String> keyAliases;
    private final LazySodiumAndroid lazySodium = new LazySodiumAndroid(new SodiumAndroid());
    private final SecretBox.Lazy secretBox = lazySodium;
    private final Box.Lazy boxLazy = lazySodium;
    private final Random random = lazySodium;
    private BiometricManager authManager;
    private Context appContext;
    private Map<String, String> currentOperation;
    private SecureCharBuffer currentContents;
    private KeyDecrypter decrypter;
    private int attempts = 0;

    private SecureCharBuffer decryptedKey;

    public SecureStore(String storageDir, Context context) throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
        ks = KeyStore.getInstance("AndroidKeyStore");
        ks.load(null);
        storagePath = storageDir;
        authManager = new BiometricManager.BiometricBuilder(context)
                .setTitle(context.getString(R.string.biometric_title))
                .setSubtitle(context.getString(R.string.biometric_subtitle))
                .setDescription(context.getString(R.string.biometric_description))
                .setNegativeButtonText(context.getString(R.string.biometric_negative_button_text))
                .build();

        appContext = context;
    }

    @Override
    public void onSdkVersionNotSupported() {
        Toast.makeText(appContext, appContext.getString(R.string.biometric_error_sdk_not_supported), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBiometricAuthenticationNotSupported() {
        Toast.makeText(appContext, appContext.getString(R.string.biometric_error_hardware_not_supported), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBiometricAuthenticationNotAvailable() {
        Toast.makeText(appContext, appContext.getString(R.string.biometric_error_fingerprint_not_available), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBiometricAuthenticationPermissionNotGranted() {
        Toast.makeText(appContext, appContext.getString(R.string.biometric_error_permission_not_granted), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBiometricAuthenticationInternalError(String error) {
        Toast.makeText(appContext, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAuthenticationFailed() {
        Toast.makeText(appContext, appContext.getString(R.string.biometric_failure), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAuthenticationCancelled() {
        Toast.makeText(appContext, appContext.getString(R.string.biometric_cancelled), Toast.LENGTH_LONG).show();
        ((Activity) appContext).finish();
        authManager.cancelAuthentication();
    }

    @Override
    public void onAuthenticationSuccessful() {
        Toast.makeText(appContext, appContext.getString(R.string.biometric_success), Toast.LENGTH_LONG).show();
        if (currentOperation == null || currentOperation.isEmpty()) {
            return;
        }

        String operation = currentOperation.get("operation");
        String fileName = currentOperation.get("fileName");
        String keyAlias = currentOperation.get("keyAlias");

        if (operation.equals("store")) {
            SecureCharBuffer contents = currentContents;
            try {
                store(fileName, contents, keyAlias);
            } catch (Exception e) {
                Log.e("RE-CRYPTO", "Failed to store key material on successful auth.");
            }
        } else if (operation.equals("load")) {
            try {
                load(fileName, keyAlias, decrypter);
                // Empty the current operation since it has executed.
                currentOperation.clear();
            } catch (Exception e) {
                Log.e("RE-CRYPTO", "Failed to reload key material on successful auth.");
            }
        }

    }

    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
//        Toast.makeText(getApplicationContext(), helpString, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {
//        Toast.makeText(getApplicationContext(), errString, Toast.LENGTH_LONG).show();
    }

    /**
     * Decrypts a value from secure storage.
     * @param fileName Name of the file that the original value was encrypted into.
     * @param keyAlias Alias for the OS secure key to use for the decryption.
     */
    public void load(String fileName, String keyAlias, KeyDecrypter keyDecrypter) throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, SodiumException, IOException {
        decrypter = keyDecrypter;
        Path target = Paths.get(storagePath, fileName);
        byte[] contents;
        contents = Files.readAllBytes(target);
        KeyPair cryptoKeys;
        String result = null;

        try {
            decryptedKey = loadEnclaveKeys(keyAlias, contents);
            keyDecrypter.onKeyDecryptSuccess(decryptedKey);
        } catch (UserNotAuthenticatedException e) {
            // Authenticate the user and then try again.
            attempts += 1;
            if (attempts < 5) {
                if (currentOperation == null || currentOperation.isEmpty()) {
                    currentOperation = new HashMap<>();
                    currentOperation.put("operation", "load");
                    currentOperation.put("fileName", fileName);
                    currentOperation.put("keyAlias", keyAlias);
                }
                authManager.authenticate(this);
            }
        } catch (Exception e) {
            keyDecrypter.onKeyDecryptFailure(e);
            Log.e("CRYPTO", "Unable to derive keys from secure enclave.");
        }
    }

    /**
     * Encrypts the master key using the secure enclave.
     * @param keyAlias Name of the key from the key store to use.
     * @return Bytes of the encrypted master key.
     */
    private String storeEnclaveKeys(String keyAlias, SecureCharBuffer masterKey) throws Exception {
        java.security.KeyPair keys = getKeyPair(keyAlias);
        Cipher c = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        c.init(Cipher.ENCRYPT_MODE, keys.getPublic());
        return Base64.getEncoder().encodeToString(c.doFinal(SecureBase58.decodeSecure(masterKey)));
    }

    /**
     * Decrypts the master key encrypted using `storeEnclaveKeys`.
     * @param keyAlias Name of the key from the key store to use.
     * @param encrypted Bytes returned by `storeEnclaveKeys`.
     * @return Secure buffer with the contents of the master key.
     */
    private SecureCharBuffer loadEnclaveKeys(String keyAlias, byte[] encrypted) throws Exception {
        java.security.KeyPair keys = getKeyPair(keyAlias);
        Cipher c = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        c.init(Cipher.DECRYPT_MODE, keys.getPrivate());
        String encString = new String(encrypted);
        byte[] decrypted = c.doFinal(Base64.getDecoder().decode(encString));
        SecureCharBuffer result = SecureBase58.encodeSecure(decrypted);
        Arrays.fill(decrypted, (byte)0);
        return result;
    }


    /**
     * Writes an encrypted copy of a string to disk using keys from secure storage.
     * @param fileName the name that the encrypted contents should be written under.
     * @param contents message to write securely to disk.
     * @param keyAlias alias of the key to use for encryption and decryption.
     */
    public void store(String fileName, SecureCharBuffer contents, String keyAlias) throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, SodiumException {
        File target = new File(storagePath, fileName);
        String encrypted = null;
        try {
            encrypted = storeEnclaveKeys(keyAlias, contents);
        } catch (UserNotAuthenticatedException e) {
            // Authenticate the user and then try again.
            currentOperation = new HashMap<>();
            currentOperation.put("operation", "store");
            currentOperation.put("fileName", fileName);
            currentContents = contents;
            currentOperation.put("keyAlias", keyAlias);
            authManager.authenticate(this);
        } catch (Exception e) {
            Log.e("CRYPTO", "Unable to derive keys from secure enclave.");
            return;
        }

        if (encrypted != null) {
            try (FileOutputStream fos = new FileOutputStream(target)) {
                fos.write(encrypted.getBytes());
            } catch (IOException e) {
                Log.e("CRYPTO", "Error saving encrypted keys to file.");
            }
        }
    }

    /**
     * Gets a keyPair from the secure storage of the OS. If it doesn't exist, it will be created.
     * @param alias to use when storing/retrieving the key.
     */
    public java.security.KeyPair getKeyPair(String alias) throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException {
        java.security.KeyPair result = null;
        PrivateKey privateKey = null;
        PublicKey publicKey = null;

        try {
            if (!ks.containsAlias(alias)) {
                KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(
                        KeyProperties.KEY_ALGORITHM_RSA, "AndroidKeyStore");
                keyPairGenerator.initialize(
                        new KeyGenParameterSpec.Builder(
                                alias, KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                                .setDigests(KeyProperties.DIGEST_SHA256,
                                        KeyProperties.DIGEST_SHA384,
                                        KeyProperties.DIGEST_SHA512)
                                // Only permit the private key to be used if the user authenticated
                                // within the last five minutes.
                                .setUserAuthenticationRequired(true)
                                .setUserAuthenticationValidityDurationSeconds(5 * 60)
                                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1)
                                .build());
                result = keyPairGenerator.generateKeyPair();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        refreshKeys();

        if (ks.containsAlias(alias)) {
            privateKey = (PrivateKey) ks.getKey(alias, null);
            publicKey = ks.getCertificate(alias).getPublicKey();
            result = new java.security.KeyPair(publicKey, privateKey);
        }

        return result;
    }

    /**
     * Refreshes the list of the available keys in the key store.
     */
    private void refreshKeys() {
        keyAliases = new ArrayList<>();
        try {
            Enumeration<String> aliases = ks.aliases();
            while (aliases.hasMoreElements()) {
                keyAliases.add(aliases.nextElement());
            }
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
    }
}
