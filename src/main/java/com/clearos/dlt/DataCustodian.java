package com.clearos.dlt;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.clearos.dlt.signing.Ed25519Signer;

import com.clearos.dlt.signing.Signature;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Headers;
import okhttp3.RequestBody;
import okhttp3.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.time.Instant;

public class DataCustodian {
    private final String hostName;
    private final int hostPort;
    private final String apiKey;
    private final String USER_AGENT = "Datacustodian/1.2.2";

    public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    OkHttpClient client = new OkHttpClient();

    private static final Logger log = LoggerFactory.getLogger(DataCustodian.class);
    private CryptoKey keys;

    public DataCustodian(String APIKey, CryptoKey cryptoKeys) {
        hostName = "decentralized.clearfoundation.com";
        hostPort = 443;
        apiKey = APIKey;
        keys = cryptoKeys;
    }

    public DataCustodian(String host, String APIKey, CryptoKey cryptoKeys) {
        hostName = host;
        hostPort = 8889;
        apiKey = APIKey;
        keys = cryptoKeys;
    }

    public DataCustodian(String host, int port, String APIKey, CryptoKey cryptoKeys) {
        hostName = host;
        hostPort = port;
        apiKey = APIKey;
        keys = cryptoKeys;
    }

    public URI getURI(String path, Map<String, String> params) throws UnsupportedEncodingException, URISyntaxException {
        return new URI("https", String.format("%s:%d", hostName, hostPort), path, getParamsString(params), null);
    }

    public URI getURI(String path) throws URISyntaxException {
        return new URI("https", String.format("%s:%d", hostName, hostPort), path, null);
    }

    /**
     * Publishes a new application key to the home server. This is used to backup the application
     * data to the secure decentralized storage.
     * @param packageName Name of the package namespace for the app.
     * @param did The zero-key DID of the application's derived key.
     * @param erster If this is a key-rotation event, the previous key in the chain.
     * @param signer The current key that should be used from now on.
     * @param ensuer The next key that will be used in the rotation operation.
     */
    public Call publishAppKey(String packageName, String did, String signerDid, DidKeys erster, DidKeys signer,
                              DidKeys ensuer, Callback callback) throws Exception {
        Map<String, Object> body = new HashMap<>();
        body.put("did", did);
        body.put("signerDid", signerDid);
        body.put("package", packageName);
        if (erster != null) {
            body.put("erster", erster.verKey);
        } else {
            body.put("erster", null);
        }
        body.put("signer", signer.verKey);
        body.put("ensuer", ensuer.verKey);
        body.put("timestamp", Instant.now().toString());

        Gson gson = new Gson();
        String json = gson.toJson(body);

        Map<String, String> params = new HashMap<>();
        URI target = getURI("/id/did/app-keys", params);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();
        headers.put("x-api-key", apiKey);
        headers.put("signer-sign", keys.sign(json, signer));
        if (erster != null) {
            headers.put("erster-sign", keys.sign(json, erster));
        }

        // If this is the first app key (i.e., erster is null) then we sign using the home master
        // key; otherwise, sign with the erster key.
        Headers headerbuild = Headers.of(headers);
        RequestBody jBody = RequestBody.create(json, JSON);
        Request request = new Request.Builder().url(target.toURL()).headers(headerbuild).
                post(jBody).build();

        if (erster != null) {
            request = keys.signRequest(request, erster);
        } else {
            request = keys.masterSignRequest(request);
        }

        // Finally make the API endpoint call.
        return doRequest(request, callback);
    }

    public Call rotateMasterKey(String did, DidKeys erster, DidKeys signer, DidKeys ensuer, Callback callback) throws Exception {
        Map<String, String> params = new HashMap<>();
        URI target = getURI("/id/did/rotate-master", params);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();
        headers.put("x-api-key", apiKey);

        // Add the verkey into the body (it is the first request, so it can't be looked up publicly); also add all the
        // pre-derived signer keys that will be added as metadata.
        Map<String,Object> body = new HashMap<>();
        body.put("did", did);
        body.put("erster", erster.verKey);
        body.put("signer", signer.verKey);
        body.put("ensuer", ensuer.verKey);
        body.put("timestamp", Instant.now().toString());

        Gson gson = new Gson();
        String json = gson.toJson(body);
        headers.put("erster-sign", keys.sign(json, erster));
        headers.put("signer-sign", keys.sign(json, signer));

        Headers headerbuild = Headers.of(headers);
        RequestBody jBody = RequestBody.create(json, JSON);
        Request request = new Request.Builder().url(target.toURL()).headers(headerbuild).
                post(jBody).build();

        // Sign the request using the *master* key.
        request = keys.masterSignRequest(request);

        // Finally make the API endpoint call.
        return doRequest(request, callback);
    }


    /**
     * Registers a master DID key with a decentralized ledger as well as 10 derived signing keys that can be used
     * to sign other requests without disclosing the master key.
     * @param did Master DID
     * @param verKey Public key associated with the Master DID.
     * @param serial Serial number of the device registering the masterKey.
     * @param serialKeys Keypair placed on the phone to use for own-your-phone.
     * @param ensuer The next encryption key that will be used for this DID and verkey at the home server.
     */
    public Call publishMasterKey(String did, String verKey, String serial, DidKeys serialKeys,
                                 DidKeys ensuer, String name, String displayName, String icon,
                                 Callback callback) throws Exception {
        Map<String, String> params = new HashMap<>();
        URI target = getURI("/id/did/master-key", params);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();
        headers.put("x-api-key", apiKey);

        // Add the verkey into the body (it is the first request, so it can't be looked up publicly); also add all the
        // pre-derived signer keys that will be added as metadata.
        Map<String,Object> body = new HashMap<>();
        body.put("did", did);
        body.put("verkey", verKey);
        body.put("serial", serial);
        body.put("serial-key", serialKeys.verKey);
        body.put("ensuer", ensuer.verKey);
        body.put("timestamp", Instant.now().toString());
        body.put("name", name);
        body.put("displayName", displayName);
        body.put("icon", icon);

        Gson gson = new Gson();
        String json = gson.toJson(body);
        headers.put("serial-sign", keys.sign(json, serialKeys));

        Headers headerbuild = Headers.of(headers);
        RequestBody jBody = RequestBody.create(json, JSON);
        Request request = new Request.Builder().url(target.toURL()).headers(headerbuild).
                post(jBody).build();

        // Sign the request using the *master* key.
        request = keys.masterSignRequest(request);
        return doRequest(request, callback);
    }

    private Call doRequest(Request request, Callback callback) {
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }

    private static String getParamsString(Map<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();

        for (Map.Entry<String, String> entry : params.entrySet()) {
            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            result.append("&");
        }

        String resultString = result.toString();
        return resultString.length() > 0 ? resultString.substring(0, resultString.length() - 1) : resultString;
    }
}