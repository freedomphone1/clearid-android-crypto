package com.clearos.dlt.testing;

import com.clearos.dlt.BuildConfig;
import com.clearos.dlt.CryptoKey;
import com.clearos.dlt.DidKeys;
import com.goterl.lazycode.lazysodium.exceptions.SodiumException;
import com.goterl.lazycode.lazysodium.utils.Key;
import com.goterl.lazycode.lazysodium.utils.KeyPair;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.Base64;

import io.github.novacrypto.SecureCharBuffer;

public class CryptoKeyTest {
    private static final Logger log = LoggerFactory.getLogger(CryptoKeyTest.class);

    @Test
    public void testSeedToWords() throws SodiumException {
        SecureCharBuffer seed = new SecureCharBuffer();
        seed.append("00000000000000000seed-new-public");
        CryptoKey ck = new CryptoKey(null, seed, false);

        SecureCharBuffer words = new SecureCharBuffer();
        words.append("coral light army gather adapt blossom school alcohol coral light army gather already razor night mad hover close slender thought file curtain pitch host");
        if (BuildConfig.DEBUG && !ck.masterSeedWords.equals(words)) {
            throw new AssertionError();
        }
    }

    @Test
    public void testWordsToSeed() throws SodiumException {
        SecureCharBuffer words = new SecureCharBuffer();
        words.append("coral light army gather adapt blossom school alcohol coral light army gather already razor night mad hover close slender thought file curtain pitch host");
        CryptoKey ck = new CryptoKey(null, words, true);

        SecureCharBuffer seed = new SecureCharBuffer();
        seed.append("00000000000000000seed-new-public");
        if (BuildConfig.DEBUG && !ck.verifyMasterSeed(seed)) {
            throw new AssertionError();
        }
    }

    @Test
    public void testRandomWords() throws SodiumException {
        CryptoKey ck = new CryptoKey(null);
    }

//    @Test
//    public void testGenerateKey() throws Exception {
//        CryptoKey ck = CryptoKey.generateMasterKey(null);
//        SecureCharBuffer words = ck.masterSeedWords;
//        ck = CryptoKey.fromSecureStorage(null);
//        if (BuildConfig.DEBUG && !ck.masterSeedWords.equals(words)) {
//            throw new AssertionError();
//        }
//    }

    @Test
    public void testRestoreWords() throws CertificateException, SodiumException, IOException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException {
        SecureCharBuffer words = new SecureCharBuffer();
        words.append("coral light army gather adapt blossom school alcohol coral light army gather already razor night mad hover close slender thought file curtain pitch host");
        CryptoKey.restoreMasterKey(null, words);
    }

    @Test
    public void testDeriveKeys() throws SodiumException {
        SecureCharBuffer seed = new SecureCharBuffer();
        seed.append("00000000000000000seed-new-public");
        CryptoKey ck = new CryptoKey(null, seed, false);

        KeyPair kp = ck.deriveKeyPair(10,"_testing" );
        if (BuildConfig.DEBUG && !kp.getPublicKey().getAsHexString().equals("EDEA923969E5E9A2AC261BC56DB9798DDB265FEC2AAABBDA02A366700346123A")) {
            throw new AssertionError();
        }
        if (BuildConfig.DEBUG && !kp.getSecretKey().getAsHexString().equals("3DDACE5DB44A30DD5035B8D0F539352FD34E58B80C967B43B446178AC237E233EDEA923969E5E9A2AC261BC56DB9798DDB265FEC2AAABBDA02A366700346123A")) {
            throw new AssertionError();
        }
    }

//    @Test
//    public void testPublishDerivedKey() throws Exception {
//        String URL = "decentralized.clearfoundation.com";
//        String APIKey = "322fef62fecf4b888232c6df185d35d6";
//        CryptoKey.publishAppKey(null, 10);
//    }

//    @Test
//    public void testPushDecrypt() throws Exception {
//        SecureCharBuffer words = new SecureCharBuffer();
//        words.append("coral light army gather adapt blossom school alcohol coral light army gather already razor night mad hover close slender thought file curtain pitch host");
//        CryptoKey.restoreMasterKey(null, words);
//
//        String notificationVerkey = "FQTn2kCNuvxsKDHvHMyci41nXH4Gds6NGFffQhyvDcsq";
//        String notificationContents = "0jrvKdZlWX0pu92gSwggF2nV8kVM0XiyuU0bKwpl/SLARX5iLk1z1zJWL9WcHJJPsEvXxvDsaACwugQNdR/bo6tTWQtYL3R1CC9cqvD4Y5th53ZhioevWZeDD5J9zA8iysURwIYsDVNSJ8HRLJMZTkmJqht75UtRIcVMibjO8GXs8lhYwYR5XQsx5Tk+E5NaE6Gy3Sw4Rk7XMuoqx1ZBtZOkfRzgO7LqkETWKpGsHA==";
//        String decrypted = CryptoKey.decryptPushNotification(null, notificationVerkey, notificationContents);
//        if (BuildConfig.DEBUG && decrypted == null) {
//            throw new AssertionError();
//        }
//        if (BuildConfig.DEBUG && decrypted.equals("{\"url\": \"datacustodian:8889\", \"fields\": [\"username\", \"password\"], \"plugin\": \"ApSDXXXdP5VosMKnqfpyYcjXzuDDb5nI8Z0CuKQgBTuTCbJu\"}")) {
//            throw new AssertionError();
//        }
//    }

    @Test
    public void testAnonCrypt() throws SodiumException {
        CryptoKey ck = new CryptoKey(null);
        String encrypted = CryptoKey.anonCrypt("Secret message", ck.getMasterPublicKey());
        String decrypted = ck.anonDecryptMaster(encrypted);
        if (BuildConfig.DEBUG && !decrypted.equals("Secret message")) {
            throw new AssertionError();
        }
    }

    @Test
    public void testVaultCryption() throws SodiumException {
        CryptoKey ck = new CryptoKey(null);
        CryptoKey dk = new CryptoKey(null);
        String vaultKey = ck.encryptVaultKey(dk.getMasterVerkey());
        String decryptKey = dk.anonDecryptMaster(vaultKey);
        // TODO: hard-code this value and add hard-coded seeds above as well.
        // assert decryptKey.equals("");
    }

    @Test
    public void testPluginPassword() throws SodiumException {
        CryptoKey ck = new CryptoKey(null);
        CryptoKey dk = new CryptoKey(null);
        String vaultKey = ck.getPluginPassword(dk.getMasterVerkey());
        String decryptKey = dk.anonDecryptMaster(vaultKey);
        // TODO: hard-code this value and add hard-coded seeds above as well.
        // assert decryptKey.equals("");
    }
}
